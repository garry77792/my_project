import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {HomePageComponent} from './home-page/home-page.component';

const appRoutes: Routes = [

  { path: '', component: HomePageComponent},
  { path: 'login', redirectTo: 'login' },
  { path: 'system', redirectTo: 'system' },
  { path: 'allUsers', redirectTo: 'allUsers' },
  { path: 'registration', redirectTo: 'registration'},
  { path: 'not-found', component: PageNotFoundComponent},
  { path: '**', redirectTo: '/not-found' },

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
