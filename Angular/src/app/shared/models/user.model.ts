export class User {

  constructor(

    public name: string,
    public password: string,
    public birthDate: string,
    public phoneNumber: number,
    public monthlySalary: number,
    public liabilities: number,
    public id?: number ){

  }
}
