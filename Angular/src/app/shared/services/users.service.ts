import {User} from '../models/user.model';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {BaseApi} from "../core/base-api";


@Injectable()
export class UsersService extends BaseApi {

  constructor(public http: Http) {
    super(http);
  }


  getUserByName(name: string): Observable<User> {
    return this.http.get(`http://localhost:3000/users?name=${name}`)
      .map((response: Response) => response.json())
      .map((users: User[]) => users[0] ? users[0] : undefined);
  }

  getAllUsersFromDb() {
    return this.get('users');
  }

  /*createNewUser(user: User): Observable<User> {
    return this.http.post('http://localhost:3000/users', user)
      .map((response: Response) => response.json())
  }*/

  createNewUser(user: User): Observable<User> {
    return this.post('users', user);
  }

  updateUser(user: User, id: number): Observable<User> {
    return this.put(`users/${id}`, user);
  }

  deleteUser(userId: number) {
    return this.http.delete(`http://localhost:3000/users/${userId}`);
    }
}
