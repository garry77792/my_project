import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "../../shared/models/user.model";
import {UsersService} from "../../shared/services/users.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit, OnDestroy {

  users: User[];
  subscription: Subscription;

  constructor( public usersService: UsersService ) { }

  ngOnInit() {
    this.subscription = this.usersService.getAllUsersFromDb().subscribe (
      (users: User[]) => this.users = users,
      (error) => console.log(error)
    );

    console.log('aaa');
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
