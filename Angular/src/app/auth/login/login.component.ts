import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";

import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/models/user.model';
import {Message} from "../../shared/models/message.model";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  message: Message;

  constructor(private usersService: UsersService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.message = new Message('danger', '');

    this.route.queryParams.subscribe((params: Params) => {
      if (params['nowCanLogin']) {
        this.showMessage({
          text: "Now You can join to the System",
          type: "success"
        });
      }
    });


    this.loginForm = new FormGroup({
      'name': new FormControl(null, [Validators.required,
        Validators.minLength(4)]),
      'password': new FormControl(null, [Validators.required,
        Validators.minLength(4)])
    });
  }

  private showMessage(message: Message) {
    this.message = message;
    window.setTimeout(() => {
      this.message.text = '';
    }, 5000);

  }

  onSubmit() {
    const formData = this.loginForm.value;
    this.usersService.getUserByName(formData.name)
      .subscribe((user: User) => {
        if (user) {
          if (user.password === formData.password) {
            this.message.text = '';
            window.localStorage.setItem('user', JSON.stringify(user));
            this.authService.login();
            this.router.navigate(['../system', 'ui']);
          } else {
            this.showMessage({
              text: "Password is incorrect",
              type: "danger"
            });
          }
        }
        else this.showMessage({
          text: "This user doesn't exist",
          type: "danger"
        });
      });
  }


}
