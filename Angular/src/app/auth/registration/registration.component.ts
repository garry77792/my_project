import {Component, OnInit} from '@angular/core';
import {UsersService} from "../../shared/services/users.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../shared/models/user.model";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  constructor(private usersService: UsersService,
              private router: Router) {}

  ngOnInit() {
    this.form = new FormGroup({
      'name': new FormControl(null,
        [Validators.required, Validators.minLength(4)], this.forbiddenNames.bind(this)),
      'password': new FormControl(null,
        [Validators.required, Validators.minLength(4)]),
      'birthDate': new FormControl(null, Validators.required),
      'phoneNumber': new FormControl(null, Validators.required),
      'monthlySalary': new FormControl(null, Validators.required),
      'liabilities': new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    const { name, password, birthDate, phoneNumber,
      monthlySalary, liabilities } = this.form.value;
    const user = new User(name, password, birthDate, phoneNumber,
      monthlySalary, liabilities);

    this.usersService.createNewUser(user)
      .subscribe(() => {
        this.router.navigate(['/login'], {
          queryParams: {
            nowCanLogin: true
          }
        });
      });
  }

  forbiddenNames(control: FormControl): Promise<any> {
    return new Promise((resolve, reject) => {
      this.usersService.getUserByName(control.value)
        .subscribe((user: User) => {
          if (user) {
            resolve({forbiddenName: true});
          } else {
            resolve(null);
          }
        });
    });
  }


  // My own BirthDate Validation

  /*forbiddenDates(control: FormControl): Promise<any> {
    return new Promise( (resolve, reject) => {
      const clientBirthDate = new Date(this.form.value.birthDate).getFullYear();
         if (clientBirthDate ) {


           resolve({forbiddenDate: true});
         } else {
           resolve(null);
         }
    });
         }*/


}
