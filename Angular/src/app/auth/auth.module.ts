import {NgModule} from '@angular/core';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import {CommonModule} from '@angular/common';
import {AuthRoutingModule} from './auth-routing.module';
import {AuthComponent} from './auth.component';
import {SharedModule} from '../shared/shared.module';
import { AllUsersComponent } from './all-users/all-users.component';

@NgModule({
  declarations: [
    RegistrationComponent,
    LoginComponent,
    AuthComponent,
    AllUsersComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule
  ]
})

export class AuthModule {

}
