import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {SystemRoutingModule} from "./system-routing.module";
import { UiComponent } from './ui/ui.component';
import { EditComponent } from './edit/edit.component';
import {SystemComponent} from "./system.component";

@NgModule({
   imports: [CommonModule, SharedModule,
     SystemRoutingModule],

   declarations: [UiComponent, EditComponent, SystemComponent]
})
export class SystemModule {

}
