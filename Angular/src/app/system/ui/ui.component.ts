import {Component, OnInit} from '@angular/core';
import {User} from "../../shared/models/user.model";
import {UsersService} from "../../shared/services/users.service";
import {AuthService} from "../../shared/services/auth.service";
import {Router} from "@angular/router";
import {Response} from "@angular/http";

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.css']
})
export class UiComponent implements OnInit {

  date: Date = new Date();
  user: User;
  customerCredit: number;

  constructor(private auth: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('user'));

    // Date here is Temporary

    /*const clientDate1 = new Date(this.user.birthDate).getFullYear();
    const today1 = new Date().getFullYear();
    const age1 = today1 - clientDate1;

    this.customerCredit = age1 * 100 + this.user.monthlySalary - this.user.liabilities;*/

    const clientDate = new Date(this.user.birthDate).getTime();
    const today = new Date().getTime();
    const age = Math.abs(Math.round((today - clientDate) / (1000 * 60 * 60 * 24 * 365.25)));

    this.customerCredit = age * 100 + this.user.monthlySalary - this.user.liabilities;
  }

  onLogout() {
    this.auth.logout();
    this.router.navigate(['/allUsers']);
  }

  onEdit() {
    this.router.navigate(['system', 'edit']);
  }

}
