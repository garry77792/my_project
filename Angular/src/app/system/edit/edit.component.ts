import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "../../shared/models/user.model";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UsersService} from "../../shared/services/users.service";
import {Subscription} from "rxjs/Subscription";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  user: User;
  message = '';

  form: FormGroup;

  constructor(private usersService: UsersService,
              private router: Router) {
  }

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('user'));
    this.form = new FormGroup({
      'name': new FormControl(this.user.name,
        [Validators.required, Validators.minLength(4)], this.forbiddenNames.bind(this)),
      'password': new FormControl(this.user.password,
        [Validators.required, Validators.minLength(4)]),
      'phoneNumber': new FormControl(this.user.phoneNumber, Validators.required),
      'monthlySalary': new FormControl(this.user.monthlySalary, Validators.required),
      'liabilities': new FormControl(this.user.liabilities, Validators.required)
    });
  }

  onSubmit() {
    const {name, password, phoneNumber, monthlySalary, liabilities} = this.form.value;
    const user = new User(name, password, this.user.birthDate, phoneNumber,
      monthlySalary, liabilities);

    this.usersService.updateUser(user, this.user.id)
      .subscribe((user: User) => {
        this.user = user;
        this.message = 'You Successfully changed your Data';
        window.scrollTo(0, 0);
      });

  }

  forbiddenNames(control: FormControl): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.user.name === control.value) {
        resolve(null);
      } else {
        this.subscription = this.usersService.getUserByName(control.value)
          .subscribe((user: User) => {
            if (user) {
              resolve({forbiddenName: true});
            } else {
              resolve(null);
            }
          });
      }
    });

  }

  toLoginPage() {
    this.router.navigate(['/system', 'ui']);
    window.localStorage.setItem('user', JSON.stringify(this.user));
  }

  onDelete(userId: number) {
    if (confirm("Are you Sure to Delete")) {
      this.usersService.deleteUser(userId).subscribe();
      this.router.navigate(["/"]);

    }
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }


}
