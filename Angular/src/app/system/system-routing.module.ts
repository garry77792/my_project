import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {SystemComponent} from "./system.component";
import {UiComponent} from "./ui/ui.component";
import {EditComponent} from "./edit/edit.component";

const routes: Routes = [

  { path: 'system', component: SystemComponent, children: [
      { path: 'ui', component: UiComponent },
      { path: 'edit', component: EditComponent }
    ]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule {

}
